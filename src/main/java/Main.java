import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

public class Main {

    public static void main(String[] args) throws IOException {

        String fileEON = "/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1E0N.lf_str.out";
        String fileHPN = "/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1HP9.lf_str.out";
        String fileHZN = "/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1HZN.lf_str.out";


        String eonAA = "000PGWEIIHENGRPLYYNAEQKTKLHYPP000".toLowerCase();
        String hznAA = "000IFSANAWRAYDTASAERRLSGTPISFILL000".toLowerCase();
        String hpnAA = "000GHACYRNCWREGNDEETCKERC000".toLowerCase();

        double[] eonRSA = {0, 0, 0, 1.30344827586207, 0.453333333333333, 0.384313725490196, 0.589473684210526, 0.702857142857143, 0.28, 0.507692307692308, 0.715789473684211, 0.80625, 0.786666666666667, 0.72, 0.648275862068965, 0.211764705882353, 0.308695652173913, 0.295652173913043, 0.15625, 0.48695652173913, 0.789473684210526, 0.877777777777778, 0.825, 0.407142857142857, 0.69, 0.205882352941176, 0.738461538461539, 0.552173913043478, 0.668965517241379, 0.717241379310345};
        double[] hpnRSA = {0, 0, 0, 1.72, 0.492307692307692, 0.373913043478261, 0.192592592592593, 0.68695652173913, 0.724444444444444, 0.36875, 0.111111111111111, 0.376470588235294, 0.728888888888889, 0.805263157894737, 1.04, 0.26875, 0.513333333333333, 0.647368421052632, 0.710526315789474, 0.45, 0.0592592592592593, 0.64, 0.726315789473684, 0.844444444444444, 0.725925925925926};
        double[] hznRSA = {0, 0, 0, 0.96, 0.8, 0.478260869565217, 0.28695652173913, 0.38125, 0.426086956521739, 0.749019607843137, 0.582222222222222, 0.373913043478261, 0.717391304347826, 0.726666666666667, 0.278571428571429, 0.104347826086957, 0.417391304347826, 0.747826086956522, 0.468421052631579, 0.675555555555556, 0.684444444444444, 0.964705882352941, 0.669565217391304, 1.34666666666667, 0.407142857142857, 0.63448275862069, 0.245714285714286, 0.643478260869565, 0.780952380952381, 0.382857142857143, 0.547058823529412, 1.51764705882353};

        CSVReader reader = new CSVReader(new FileReader("/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1E0N.csv"));
        List eonDistribution = reader.readAll();

        reader = new CSVReader(new FileReader("/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1HP9.csv"));
        List hpnDistribution = reader.readAll();

        reader = new CSVReader(new FileReader("/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1HZN.csv"));
        List hznDistribution = reader.readAll();

//        processInput(fileEON, eonAA, eonRSA, "dataset1E0N", 3, eonDistribution);
//        processInput(fileHPN, hpnAA, hpnRSA, "dataset1HP9", 3, hpnDistribution);
//        processInput(fileHZN, hznAA, hznRSA, "dataset1HZN", 3, hznDistribution);

        processInputForClassification(fileEON, eonAA, eonRSA, "1E0NTainingSet", 3, eonDistribution);

    }


    private static void processInput(String filename, String aa, double[] rsa, String outputFile, int window, List distribution) throws IOException {
        File file = new File(filename);
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        String previousLine = "";
        String previousPreviousLine = "";
        File fout = new File(outputFile);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        while ((line = bufferedReader.readLine()) != null) {
            line = "000" + line + "000";
            if (previousLine.equals("")) {
                for (int i = 0; i < line.length(); i++) {
                    previousLine = previousLine + "0";
                    previousPreviousLine = previousPreviousLine + "0";
                }
            }

            // process line
            for (int i = window; i < line.length() - window; i++) {
                bw.write((int) previousPreviousLine.charAt(i - 3) + "," + (int) previousPreviousLine.charAt(i - 2) + "," + (int) previousPreviousLine.charAt(i - 1) + "," + (int) previousPreviousLine.charAt(i) + "," + (int) previousPreviousLine.charAt(i + 1) + "," + (int) previousPreviousLine.charAt(i + 2) + "," + (int) previousPreviousLine.charAt(i + 3) + "," +
                        (int) previousPreviousLine.charAt(i - 3) + "," + (int) previousLine.charAt(i - 2) + "," + (int) previousLine.charAt(i - 1) + "," + (int) previousLine.charAt(i) + "," + (int) previousLine.charAt(i + 1) + "," + (int) previousLine.charAt(i + 2) + "," + (int) previousLine.charAt(i + 3) + "," +
                        (int) previousPreviousLine.charAt(i - 3) + "," + (int) line.charAt(i - 2) + "," + (int) line.charAt(i - 1) + "," + (int) line.charAt(i) + "," +
                        (int) aa.charAt(i + 1) + "," + rsa[i + 1] + "," + getLineForLetter(line.charAt(i),distribution, i-2));
                bw.newLine();

            }
            previousPreviousLine = previousLine;
            previousLine = line;

        }
        bw.close();
        fileReader.close();

    }

    private static void processInputForClassification(String filename, String aa, double[] rsa, String outputFile, int window, List distribution) throws IOException {
        File file = new File(filename);
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        String previousLine = "";
        String previousPreviousLine = "";
        File fout = new File(outputFile);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        while ((line = bufferedReader.readLine()) != null) {
            line = "000" + line + "000";
            if (previousLine.equals("")) {
                for (int i = 0; i < line.length(); i++) {
                    previousLine = previousLine + "0";
                    previousPreviousLine = previousPreviousLine + "0";
                }
            }

            // process line
            for (int i = window; i < line.length() - window; i++) {
                bw.write((int) previousPreviousLine.charAt(i - 3) + "," + (int) previousPreviousLine.charAt(i - 2) + "," + (int) previousPreviousLine.charAt(i - 1) + "," + (int) previousPreviousLine.charAt(i) + "," + (int) previousPreviousLine.charAt(i + 1) + "," + (int) previousPreviousLine.charAt(i + 2) + "," + (int) previousPreviousLine.charAt(i + 3) + "," +
                        (int) previousPreviousLine.charAt(i - 3) + "," + (int) previousLine.charAt(i - 2) + "," + (int) previousLine.charAt(i - 1) + "," + (int) previousLine.charAt(i) + "," + (int) previousLine.charAt(i + 1) + "," + (int) previousLine.charAt(i + 2) + "," + (int) previousLine.charAt(i + 3) + "," +
                        (int) previousPreviousLine.charAt(i - 3) + "," + (int) line.charAt(i - 2) + "," + (int) line.charAt(i - 1) + "," +
                        (int) aa.charAt(i + 1) + "," + rsa[i + 1] + "," + getLineForLetter(line.charAt(i),distribution, i-2) + "," + line.charAt(i));
                bw.newLine();

            }
            previousPreviousLine = previousLine;
            previousLine = line;

        }
        bw.close();
        fileReader.close();

    }

    private static String getLineForLetter(char letter, List distribution, int possition){
        for (Object aDistribution : distribution) {
            String[] array = (String[]) aDistribution;
            if (String.valueOf(letter).equals(array[0])) {
                return array[possition];
            }
        }

        return null;
    }

}
