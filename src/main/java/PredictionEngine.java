
import java.io.BufferedReader;
import java.io.FileReader;

import weka.classifiers.Classifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Standardize;

/**
 * Created by silvana.albert on 3/12/17.
 */
public class PredictionEngine {
    public static void main(String[] args) throws Exception {

        Classifier j48tree = new J48();
        Instances train = new Instances(new BufferedReader(new FileReader("/Users/silvana.albert/Desktop/ProteinApp/src/main/java/sampleData.arff")));
        int lastIndex = train.numAttributes() - 1;

        train.setClassIndex(lastIndex);

        Instances test = new Instances(new BufferedReader(new FileReader("/Users/silvana.albert/Desktop/ProteinApp/src/main/java/sampleDataTest.arff")));
        test.setClassIndex(lastIndex);

        Standardize filter = new Standardize();
        filter.setInputFormat(train);  // initializing the filter once with training set
        Instances newTrain = Filter.useFilter(train, filter);  // configures the Filter based on train instances and returns filtered instances
        Instances newTest = Filter.useFilter(test, filter);    // create new test set

        j48tree.buildClassifier(train);

        for(int i=0; i<test.numInstances(); i++) {

            double index = j48tree.classifyInstance(test.instance(i));
            String className = train.attribute(lastIndex).value((int)index);
            System.out.println(className);
        }
    }
}
