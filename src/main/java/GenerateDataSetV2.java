import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by silvana.albert on 3/9/17.
 */
public class GenerateDataSetV2 {
    public static void main(String[] args) throws IOException {
        String aa1BHU = "APSCPAGSLCTYSGTGLSGARTVIPASDMEKAGTDGVKLPASARSFANGTHFTLRYGPARKVTCVRFPCYQYATVGKVAPGAQLRSLPSPGATVTVGQDLGD".toLowerCase();
        String aa1GO1 = "GSVDFAFELRKAQDTGKIVMGARKSIQYAKMGGAKLIIVARNARPDIKEDIEYYARLSGIPVYEFEGTSVELGTLLGRPHTVSALAVVDPGESRILALGGKE".toLowerCase();
        String aa1JT8 = "MAEQQQEQQIRVRIPRKEENEILGIIEQMLGASRVRVRCLDGKTRLGRIPGRLKNRIWVREGDVVIVKPWEVQGDQKCDIIWRYTKTQVEWLKRKGYLDELL".toLowerCase();
        String aa1L3P = "IPAGELQIIDKIDAAFKVAATAAATAPADDKFTVFEAAFNKAIKETTGGAYDTYKCIPSLEAAVKQAYAATVAAAPQVKYAVFEAALTKAITAMSEVQKVSQ".toLowerCase();
        String aa1P1L = "MHNFIYITAPSLEEAERIAKRLLEKKLAACVNIFPIKSFFWWEGKIEAATEFAMIVKTRSEKFAEVRDEVKAMHSYTTPCICAIPIERGLKEFLDWIDETVE".toLowerCase();
        String aa1TUL = "GTPDIIVNAQINSEDENVLDFIIEDEYYLKKRGVGAHIIKVASSPQLRLLYKNAYSTVSCGNYGVLCNLVQNGEYDLNAIMFNCAEIKLNKGQMLFQTKIWR".toLowerCase();

        String file1BHU = "/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1BHU.lf_str.out";
        String file1GO1 = "/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1GO1.lf_str.out";
        String file1JT8 = "/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1JT8.lf_str.out";
        String file1L3P = "/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1L3P.lf_str.out";
        String file1P1L = "/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1P1L.lf_str.out";
        String file1TUL = "/Users/silvana.albert/Desktop/ProteinApp/src/main/java/1TUL.lf_str.out";

        processInput(file1BHU, aa1BHU, "data1BHU");
        processInput(file1GO1, aa1GO1, "data1GO1");
        processInput(file1JT8, aa1JT8, "data1JT8");
        processInput(file1L3P, aa1L3P, "data1L3P");
        processInput(file1P1L, aa1P1L, "data1P1L");
        processInput(file1TUL, aa1TUL, "data1TUL");
    }
    private static void processInput(String filename, String aa, String outputFile) throws IOException {
        File file = new File(filename);
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        File fout = new File(outputFile);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        while ((line = bufferedReader.readLine()) != null) {
            // process line
            for (int i = 0; i < line.length(); i++) {
                bw.write(line.charAt(i) + " " + aa.charAt(i) + " " +  aa.charAt(i+1) + " " +  aa.charAt(i+2) + " " +  aa.charAt(i+3));
                bw.newLine();
            }
        }
        bw.close();
        fileReader.close();
    }
}
